import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CadastroPedidosPage } from './cadastro-pedidos.page';

const routes: Routes = [
  {
    path: '',
    component: CadastroPedidosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CadastroPedidosPageRoutingModule {}
