import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CadastroPedidosPageRoutingModule } from './cadastro-pedidos-routing.module';

import { CadastroPedidosPage } from './cadastro-pedidos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CadastroPedidosPageRoutingModule
  ],
  declarations: [CadastroPedidosPage]
})
export class CadastroPedidosPageModule {}
