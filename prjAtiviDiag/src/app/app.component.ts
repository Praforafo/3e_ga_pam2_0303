import { Component } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
    { title: 'Motos-TDS', url: '/folder/Motos-TDS', icon: '' },
    { title: 'Novas', url: '/folder/Novas', icon: '' },
    { title: 'Usadas', url: '/folder/Usadas', icon: '' },
    { title: 'Curtidas', url: '/folder/Curtidas', icon: '' },
    { title: 'Chat', url: '/folder/Chat', icon: '' },
  ];
  public labels = ['Naked', 'Carenada', 'Scooter', 'Trail', 'Touring'];
  constructor() {}
}
